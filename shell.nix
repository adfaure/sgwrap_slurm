{
  pkgs ? import (fetchTarball {
    name = "pkgs";
    url = "https://github.com/NixOS/nixpkgs/archive/20.03.tar.gz";
    sha256 = "sha256:0182ys095dfx02vl2a20j1hz92dx3mfgz2a6fhn31bqlp1wa8hlq";
  }) { },
  kapack ? import
   (fetchTarball "https://github.com/oar-team/kapack/archive/master.tar.gz")
   {},
   sgwrap ? import
   (../sgwrap)
   # (fetchTarball https://framagit.org/simgrid/sgwrap/-/archive/master/sgwrap-master.tar.gz)
   { inherit kapack; },
}:
with pkgs;
let
  mySlurm = callPackage ./slurm { };
in
pkgs.mkShell rec {

  buildInputs = with pkgs; [
    kapack.simgrid
    meson
    ninja
    cmake
    pkgconfig
    sgwrap.rsg_local
    valgrind
    mySlurm
    (python3.withPackages (ps: with ps; with python3Packages; [ docopt ]))
  ];

  # emplacement for sgwrap libraries

  SGWRAP_SOCKET_WRAPPER="${sgwrap.socket_wrapper}/lib";
  SGWRAP_LIB_FOLDER="${sgwrap.sgwrap}/lib";
}
