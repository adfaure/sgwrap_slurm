# SGWRAP with torrent experiment

To install all the software needed for the experiment one can use:

```
nix-shell shell.nix
```

# Slurm on RSG

To run the project:

```
# run.sh runs the rsg server and outputs the command to execute to run slurmctld
./run.sh
```

Once you ran the slurm command, you can use `rgs start` to start the simulation.
Don't forget to create (or change) the socket_wrapper directory.
Default to [/tmp/sgwrap]

## Journal

### Fri. 17. Jul 2020

I disabled every mutexes and condvars of slurm by directly returning 0 from the mutexes functions in sgwrap.
RSG serializes the calls, so this should not be an issue.

- The current code runs until the function `_fd_writeable` in the file `common/log.c`.
  This function calls poll with POLLOUT.
  FIXED: This was a bug in my code where poll was called recursively.

- Dlopen fails to load a plugin because it is linking the plugin with an old libc.
  One workaround is to specify the correct libc to use, using `LD_LIBRARY_PATH`.
  This is currently donne into the wrapper.py.
  To find the correct libc, one can use readelf on the plugin path (given when slurm fails to load it)

- In slurmctld (`controller.c`) I removed a loop that closes all fd.

- In slurmd (`controller.c`) I removed a loop that closes all fd.

- Slurmd + `thread_wrapper` does not work atm.
  So I run slurmd without thread wraper until it is the limit point.
  - In `slurm/default.nix` I remove numactl from buildInputs, becauses it causes the activation of `thread_wrapper` to fail.
    My guess is that numalib define an __attribute__(constructor) doing a syscall.

- Slurmd fails at finding hosts, I need to implement `gethostbyname` in `socket_wrapper`.
  To do that, I need to add the `get_property` in RSG.
  It exists in SG: http://simgrid.gforge.inria.fr/simgrid/3.20/doc/classsimgrid_1_1s4u_1_1Host.html#a5306d3c64bf5147a4408c2e72799b10e

- I discretely put a systematic sleep of 0.00001 second in RSG. (in function `handle_decision` in simulation.cpp)
  Otherwise the simulation never progresses.

- In simunix, we add the RSG option to start an actor at a specific time in the simulation.
  Without this, I put a sleep of 5 seconds in the main of slurmd, so the slurmd does crash because it could not find the controller.

- SlurmctlD stops somewhere (but doesn't crash), just after calling a poll.
  I need to investigate with gdb.

- I set the -enable-developer configure flag in the nix. But I have to remove the associated -Werror as I am also hacking the code.
  This should allow me to have the debug symbols.
  And also removed -Wall from ac_debug flag activated.
  - I can add to the postConfigure a sed call that removed -Werror into the file config.status

