from math import *


num_host = 10

plat_file = "platform_10h_c.xml"

deploy_file = "deploy_10h_sinfo.xml"

CMAKE_BINARY_DIR = "/home/adfaure/projects/simunix/build"
#SLURM_INST = "/home/davandg/bull/slurm.inst.sim"
#SLURM_INST = "/home/davandg/bull/slurm.inst.1508"
SLURM_INST = "/opt/slurm"
CMAKE_SOURCE_DIR = "/home/adfaure/projects/simunix"
workload = CMAKE_SOURCE_DIR+"/slurm/trace_sinfo.swf"

#### //end of config

simunix = CMAKE_BINARY_DIR+"/bin/simunix"
slurmctld = SLURM_INST+"/sbin/slurmctld"
slurmd = SLURM_INST+"/sbin/slurmd"
launcher = CMAKE_SOURCE_DIR+"/slurm/launcher"


hosts_id = range(0, num_host)


def host2IP(i):
    return "10.0."+str(int(i/255))+"."+str(i % 255)


def coordCircle(i):
    r = 1000.0
    a = 2.0*pi*float(i)/float(num_host)
    return "0 "+str(r*cos(a))+" "+str(r*sin(a))


#header ="""<?xml version='1.0'?>
#<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
#<platform version="4">
#<config id="General">
	#<prop id="network/coordinates" value="yes"></prop>
	#<!-- Reduce the size of the stack_size. On huge machine, if the stack is too big (8Mb by default), Simgrid fails to initiate.
	#See http://lists.gforge.inria.fr/pipermail/simgrid-user/2015-June/003745.html-->
        #<prop id="contexts/stack_size" value="16"></prop>
        #<prop id="contexts/guard_size" value="0"></prop>
#</config>

#<AS id="AS0" routing="Vivaldi">
#"""


#hosts = "".join(["""
    #<host id="host"""+str(i)+"""" coordinates=\""""+coordCircle(i)+"""\" core="4" speed="8095000000f">
      #<prop id="ip" value=\""""+host2IP(i)+"""\"/>
    #</host>""" for i in hosts_id])



#footer = """
#</AS>
#</platform>
#"""



header ="""<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4">
  <AS  id="AS0"  routing="Full">
    <host id="localhost" core="4" speed="8095000000f">
      <prop id="ip" value="127.0.0.1"/>
    </host>
  """

hosts = "".join(["""
    <host id="host"""+str(i)+"""" core="4" speed="8095000000f">
      <prop id="ip" value=\""""+host2IP(i)+"""\"/>
    </host>""" for i in hosts_id])

all_hosts = ["host"+str(i) for i in hosts_id]+["localhost"]


def combinations(iterable, r):
    # combinations('ABCD', 2) --> AB AC AD BC BD CD
    # combinations(range(4), 3) --> 012 013 023 123
    pool = tuple(iterable)
    n = len(pool)
    if r > n:
        return
    indices = list(range(r))
    yield tuple(pool[i] for i in indices)
    while True:
        for i in reversed(range(r)):
            if indices[i] != i + n - r:
                break
        else:
            return
        indices[i] += 1
        for j in range(i+1, r):
            indices[j] = indices[j-1] + 1
        yield tuple(pool[i] for i in indices)

routes = """
    <link id="link1" bandwidth="125000000Bps" latency="0.000100ms"/>"""
for (i,j) in combinations(all_hosts,2):
        if i != j:
            routes += """
<route src=\""""+i+"""\" dst=\""""+j+"""\"><link_ctn id="link1"/></route>"""


footer = """
  </AS>
</platform>
"""





fp = open(plat_file, "w")
fp.write(header)
fp.write( hosts)
fp.write( routes)
fp.write( footer)
fp.close()




header = """<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4">"""
#localhost = """
  #<process host="localhost" function="localhost_slurmctld">
     #<argument value=\""""+simunix+"""\"/>
     #<argument value=\""""+slurmctld+"""\"/>
     #<argument value="-D"/>
     #<argument value="-c"/>
  #</process>
  #<process host="localhost" function="localhost_launcher" start_time="1000">
     #<argument value=\""""+simunix+"""\"/>
     #<argument value=\""""+launcher+"""\"/>
     #<argument value=\""""+SLURM_INST+"""\"/>
     #<argument value=\""""+workload+"""\"/>
  #</process>
#"""
localhost = """
  <process host="localhost" function="localhost_slurmctld">
     <argument value=\""""+simunix+"""\"/>
     <argument value=\""""+slurmctld+"""\"/>
     <argument value="-D"/>
     <argument value="-c"/>
  </process>
  <process host="localhost" function="localhost_launcher" start_time="300">
     <argument value=\""""+simunix+"""\"/>
     <argument value="/home/adfaure/projects/simunix/slurm/launcher.sh"/>
  </process>
"""

hosts="".join(["""
  <process host="host"""+str(i)+"""" function="slurmd_dummy"""+str(i)+"""">
     <argument value=\""""+simunix+"""\"/>
     <argument value=\""""+slurmd+"""\"/>
     <argument value="-D"/>
     <argument value="-N"/>
     <argument value="dummy"""+str(i)+""""/>
  </process>
""" for i in hosts_id])

footer = """
</platform>
"""


fp = open(deploy_file, "w")
fp.write(header)
fp.write( localhost)
fp.write( hosts)
fp.write( footer)
fp.close()


