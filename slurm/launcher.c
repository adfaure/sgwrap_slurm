#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <sys/time.h>

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <pthread.h>

#include <stdio.h>
#include <unistd.h>
#include <errno.h>


#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_APPLE   "\x1b[38;5;112m"
#define ANSI_COLOR_RESET   "\x1b[0m"

long int gettimeofday_li() {
        struct timeval tp;
        gettimeofday(&tp, NULL);
        return tp.tv_sec;
}

#define print(fmt, ...) \
        do {fflush(stdout);fflush(stderr); fprintf(stdout, ANSI_COLOR_APPLE "[%li]Launcher: " fmt ANSI_COLOR_RESET "",  gettimeofday_li(), ##__VA_ARGS__);fflush(stdout);fflush(stderr); } while (0)


char* path_self = NULL;
char* slurm_inst = NULL;



#define ARGV_JOB_FILE 2
#define ARGV_SBATCH_PATH 0
char* argvs_sbatch[]= {"/path/to/sbatch", "-vvvvvvv", "batch.slurm.job", NULL};

void launch_sbatch_sleep(int jobid, double realruntime, double nproc, double reqtime) {
    
    char* cwd = ".";
    
    char sbatch[1024];
    sprintf(sbatch, "%s/job_%i.slurm.job", cwd, jobid);
    
    FILE *f = fopen(sbatch, "w");
    if (f == NULL)
    {
        print("Error opening file!\n");
        exit(1);
    }

    fprintf(f, "#!/bin/bash\n");
    fprintf(f, "#SBATCH -J job_%i\n", jobid);
    fprintf(f, "#SBATCH -n %u\n", (unsigned)nproc);
    fprintf(f, "#SBATCH -t 0:%i\n", (int)ceil(reqtime));
    fprintf(f, "#SBATCH -o %s/job_%i.out\n", cwd, jobid);
    //fprintf(f, "#SBATCH --uid=user#{user_id}\n");
    //fprintf(f, "#SBATCH --comment=\"energy:#{energy}\"\n" );
    fprintf(f, "\n");
    fprintf(f, "%s --time %f\n", path_self, realruntime);
    fprintf(f, "\n");
    fprintf(f, "exit 0\n");
    fclose(f);
    
    fflush(stdout);
    fflush(stderr);
    
    pid_t pid = fork();
    if (pid == 0) { // child process
    
        fflush(stdout);
        fflush(stderr);
        
        argvs_sbatch[ARGV_JOB_FILE] = sbatch;
        
        print("EXEC: %s %s %s\n", argvs_sbatch[0], argvs_sbatch[1], argvs_sbatch[2]);
        
        int ret = execve(argvs_sbatch[0], argvs_sbatch, environ);
        print("ERROR IN EXECVE ! ret:%i %i %s\n", ret, errno, strerror(errno));
        exit(1);
    } else if (pid > 0) { // parent process
        print("parent // pid: %i\n", pid);
    } else { // fork failed
        print("fork() failed!\n");
        exit(1);
        return;
    }
}



void launch_sinfo(int jobid, double realruntime, double nproc, double reqtime) {
    (void)realruntime;
    (void)nproc;
    (void)reqtime;
    //int     fd[2];
    pid_t   childpid;
    //char    readbuffer[1024];
    
    
    /*char sinfo_out[1024];
    sprintf(sinfo_out, "./sinfo_%i.out", jobid);
    FILE *outf = fopen(sinfo_out, "w");
    if (outf == NULL)
    {
        print("Error opening file!\n");
        exit(1);
    }
    
    
    pipe(fd);*/
    
    
            
        long int t = gettimeofday_li();
        FILE *f = fopen("/tmp/job.out", "a");
        if (f == NULL)
        {
            print("Error opening file!\n");
            exit(1);
        }
        fprintf(f, "A Job with pid %i has write something to the file at %li, now sinfo\n", getpid(), t);
        fclose(f);
    
    
    
    if((childpid = fork()) == -1)
    {
        perror("fork");
    }else if(childpid == 0)
    {
        /* Child process closes up input side of pipe */
        /*close(fd[0]);
        sleep(1);
        dup2(fd[1], fileno(stdout));
        dup2(fd[1], fileno(stderr));*/
        char sinfo_cmd[1024];
        sprintf(sinfo_cmd, "%s/bin/sinfo", slurm_inst);
        char *newargv[] = { sinfo_cmd, "-vvvvvvv", "-Nl", NULL };
        execv(newargv[0], newargv);
        print("execv failed for client %s\n", sinfo_cmd);
        print("[PARENT]This sould never  printed\n");
        exit(1);
    }
    else
    {
        /* Parent process closes up output side of pipe */
        //close(fd[1]);
        /* Read in a string from the pipe */
       /* while(read(fd[0], readbuffer, sizeof(readbuffer)) != 0){
            fprintf(outf, "%s", readbuffer);
        }
        fclose(outf);*/
    }
    
    return;
}






typedef void (*launch_func)(int jobid, double realruntime, double nproc, double reqtime);
int launch_funcs_len = 2;
launch_func launch_funcs[] = {
    launch_sbatch_sleep,
    launch_sinfo
};
















void print_me(int jobid, double subtime, double realruntime, double nproc, double reqtime, int job_type) {
    print("[JOB]: id:%i, sub:%f rt:%f nproc:%f reqt:%f type:%i\n", jobid, subtime, realruntime, nproc, reqtime, job_type);
}

double get_first_token(char*line) {
    char * pch = strtok(line," ,.-");
    double ret = strtod(pch, NULL);
    return ret;
}

double get_next_token() {
    char *pch = strtok(NULL, " \t\n");
    double ret = strtod(pch, NULL);
    return ret;
}


void usage_and_quit() {
        print("Usage: ./launcher $SLURM_INST workload.swf\n");
        exit(1);
}


void sleep_double(double t) {
        struct timespec rqtp;
        rqtp.tv_sec = t;
        rqtp.tv_nsec = (t-(double)rqtp.tv_sec)*1000.0*1000.0*1000.0;
    
        nanosleep(&rqtp, NULL);
}






int main(int argc, char **argv)
{
    if(argc < 2) {
        usage_and_quit();
    }
    
    if(strcmp("--date", argv[1]) == 0) {
        printf("%li\n", gettimeofday_li());
        exit(0);
    }
    
    if(argc == 3 && strcmp("--time", argv[1]) == 0) {
        double sleept = strtod(argv[2], NULL);
        
        long int t = gettimeofday_li();
        FILE *f = fopen("/tmp/job.out", "a");
        if (f == NULL)
        {
            print("Error opening file!\n");
            exit(1);
        }
        fprintf(f, "A Job with pid %i has write something to the file at %li, now sleep for %f\n", getpid(), t, sleept);
        fclose(f);
        
        
        print("LAUNCHER-EXEC: Before sleep %f\n", sleept);
        sleep_double(sleept);
        print("LAUNCHER-EXEC: After sleep %f\n", sleept);
        exit(0);
    }
    
    if(argc != 3) {
        usage_and_quit();
    }
    
    path_self = argv[0];
    slurm_inst = argv[1];
    char sbatch_cmd[1024];
    sprintf(sbatch_cmd, "%s/bin/sbatch", slurm_inst);
    argvs_sbatch[ARGV_SBATCH_PATH] = sbatch_cmd;
    
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen(argv[2], "r");
    if (fp == NULL) {
        print("fopen '%s': \n", argv[2]);
        perror("Launcher: ");
        exit(EXIT_FAILURE);
    }

    double prec_subtime = 0;
    
    while ((read = getline(&line, &len, fp)) != -1) {
        //print("Retrieved line of length %zu :\n", read);
        //print("%s", line);
        if(line[0] == ';')
            continue;

        // 1. Job Number
        int jobid = (int)get_first_token(line);
        // 2. Submit Time
        double subtime = get_next_token();
        // 3. Wait Time
        (void) get_next_token();
        // 4. Run Time
        double realruntime = get_next_token();
        // 5. Number of Allocated Processors
        double nproc = get_next_token();
        // 6. Average CPU Time Used
        (void) get_next_token();
        // 7. Used Memory 
        (void) get_next_token();
        // 8. Requested Number of Processors.
        (void) get_next_token();
        // 9. Requested Time.
        double reqtime = get_next_token();
        // 10. Requested Memory
        (void) get_next_token();
        // 11. Status
        (void) get_next_token();
        // 12. User ID
        (void) get_next_token();
        // 13. Group ID
        (void) get_next_token();
        // 14. Executable (Application) Number
        int job_type = (int)get_next_token();
        // 15...16...
        
        if( subtime - prec_subtime < 0.0) {
            print("ERROR: Jobs of the SWF files are not in submit time order\n");
            exit(1);
        }
        print("SLEEP: %f\n", subtime - prec_subtime);
        sleep_double(subtime - prec_subtime);
        
        
        print_me(jobid, subtime, realruntime, nproc, reqtime, job_type);
        if(job_type<0 || job_type>=launch_funcs_len ) {
            print("Error:Unknown job type. Exiting.\n");
            return 1;
        }
        launch_funcs[job_type](jobid, realruntime, nproc, reqtime);
        
        prec_subtime = subtime;

    }

    fclose(fp);
    if (line){
        free(line);
    }
    
    /*
    wait() is not supported by simunix!
    print("Wait for children to finish\n");
    int wstatus=0;
    do {
        pid_t pid = wait(&wstatus);
        print("Process %i finished\n", pid);
    } while(errno != ECHILD)
    */

    return 0;
}
