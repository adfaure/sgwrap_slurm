#this is not a real bash script
exit()


# Some notes to start slurm

## Prepare slurm

#16-05-03
SLURM_INST="/home/davandg/bull/slurm.inst.sim"
#slurm-15-08-9-1
SLURM_INST="/home/davandg/bull/slurm.inst.1508"
#slurm-2-6-9-1
SLURM_INST="/home/davandg/bull/slurm.inst.26"

### install / compile slurm

git clone git://github.com/SchedMD/slurm.git

git checkout slurm-16-05-3-1


./configure --prefix=$SLURM_INST --enable-developer --enable-multiple-slurmd 
make -j 4
make -j 4 install


### copy config

cp slurm.cert slurm.key slurm.conf $SLURM_INST/etc
replace "/home/davandg/bull/slurm.inst.sim/" by $SLURM_INST in slurm.conf

( slurm.cert and slurm.key have been generated following http://slurm.schedmd.com/quickstart_admin.html )

mkdir $SLURM_INST/tmp
mkdir $SLURM_INST/etc



## Start slurm IRL

As root.

SLURM_CONF="$SLURM_INST/etc/slurm.conf"
cd $SLURM_INST/sbin
./slurmctld -D -vvvvvvvvv -c

SLURM_CONF="$SLURM_INST/etc/slurm.conf"
cd $SLURM_INST/sbin
./slurmd -D -vvvvvvvvv -N dummy1

You can now start jobs
SLURM_CONF="$SLURM_INST/etc/slurm.conf"
cd $SLURM_INST/bin
./srun -N 1 hostname

It is convinient to chown files of tmp/:
chown -R davandg $SLURM_INST/tmp/*



## Start slurm in simulator

As root.

cp deploy.xml.in deploy.xml
edit deploy.xml, replace @CMAKE_BINARY_DIR@ and $SLURM_INST by the right values.

SLURM_CONF="$SLURM_INST/etc/slurm.conf"
../build/bin/simunix_starter.sh /home/davandg/tmp/ldpre/simunix/tests/platforms/two_hosts_platform.xml /home/davandg/tmp/ldpre/simunix/slurm/deploy.xml

../build/bin/simunix_starter.sh /home/davandg/tmp/ldpre/simunix/tests/platforms/three_hosts_platform.xml /home/davandg/tmp/ldpre/simunix/slurm/deploy.xml



## Start launcher IRL
In simunix/slurm.

gcc -Wall -Wextra -lm launcher.c -o launcher

./launcher $SLURM_INST/bin/srun trace_4jobs_1n.swf


## Rest logs
echo "" > slurmctld.log; echo "" > slurmd.dummy1.log;  echo "" > slurmSched.log ;echo "">storage.txt
echo "" > ../tmp/slurmctld.log; echo "" > ../tmp/slurmd.dummy1.log;  echo "" > ../tmp/slurmSched.log ;echo "">../tmp/storage.txt









