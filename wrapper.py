#!/usr/bin/env python

"""sgwrap wrapper.

Usage:
   wrapper.py [-stTp] [--socket --process --thread --time]
   [-D=<debug_level> -i=<iface> -d=<sw_dir>] [--]
   <command_line>...

Options:
  -h --help                 Show this screen.
  --version                 Show version.
  -s --socket               Activate socket wrapper
  -p --process              Activate process wrapper
  -t --time                 Activate time wrapper
  -T --thread               Activate thread wrapper
  -d --sw_dir=<sw_dir>      SW socket dir [default: /tmp/sgwrap]
  -i --sw_iface=<sw_iface>  SW interface [default: 10]
  -D --sw_dgb=<sw_dgb>      SW debug level [default: 0]
"""

missing_libraries_error_message = """
One sgwrap library is missing, it may be because you have forgotten to activate the shell.nix file that explicitely sets the variables SGWRAP_SOCKET_WRAPPER and SGWRAP_LIB_FOLDER. These two variables shall point to the folder containing to the `.so` files. If you dont want (or cant) use nix, you can manually set these environment variables.
"""

script_template ="""\
export SOCKET_WRAPPER_DIR="{sw_dir}"
export SOCKET_WRAPPER_DEFAULT_IFACE={sw_iface}
export SOCKET_WRAPPER_DEBUGLEVEL={sw_dgb}
export LD_LIBRARY_PATH="/nix/store/xg6ilb9g9zhi2zg1dpi4zcp288rhnvns-glibc-2.30/lib"
export LD_PRELOAD="{ld_preload}"

{command}
"""

from docopt import docopt
import os
import subprocess
import tempfile

if __name__ == '__main__':
  # Docopt arguments
  arguments = docopt(__doc__, version='sgwrap launcher 0.0')
  print(arguments)
  # Find the path of each libraries
  socket_wrapper_path_lib = os.path.join(
          os.environ['SGWRAP_SOCKET_WRAPPER'], "libsocket_wrapper.so.0.1.13")

  thread_wrapper_path_lib = os.path.join(
          os.environ['SGWRAP_LIB_FOLDER'], "libthread.so")

  time_wrapper_path_lib = os.path.join(
          os.environ['SGWRAP_LIB_FOLDER'], "libtime_wrapper.so")

  process_wrapper_path_lib = os.path.join(
          os.environ['SGWRAP_LIB_FOLDER'], "libprocess.so")

  init_wrapper_path_lib = os.path.join(
          os.environ['SGWRAP_LIB_FOLDER'], "libsgwrap_init.so")

  # Check for missing lib
  missing = False
  if not os.path.exists(socket_wrapper_path_lib):
    print(socket_wrapper_path_lib, "does not exist")
    missing = True
  if not os.path.exists(thread_wrapper_path_lib):
    print(thread_wrapper_path_lib, "does not exist")
    missing = True
  if not os.path.exists(time_wrapper_path_lib):
    print(time_wrapper_path_lib, "does not exist")
    missing = True
  if not os.path.exists(process_wrapper_path_lib):
    print(process_wrapper_path_lib, "does not exist")
    missing = True
  if not os.path.exists(init_wrapper_path_lib):
    print(init_wrapper_path_lib, "does not exist")
    missing = True

  # If one lib is missing, print error msg and exit
  if missing:
    print(missing_libraries_error_message)
    exit(-1)

  # Copy the current system environment
  env = os.environ.copy()

  # Activated libraries need to be present into the LD_PRELOAD env variable
  # The init lib always need to be loaded
  ld_preload = [ init_wrapper_path_lib ]

  # Add libraries according to input parameters
  for arg in arguments:
    if arg == "--time" and arguments[arg] == 1:
      print("time wrapping activated")
      ld_preload.append(time_wrapper_path_lib)

    if arg == "--socket" and arguments[arg] == 1:
      print("socket wrapping activated")
      ld_preload.append(socket_wrapper_path_lib)

    if arg == "--thread" and arguments[arg] == 1:
      print("thread wrapping activated")
      ld_preload.append(thread_wrapper_path_lib)

    if arg == "--process" and arguments[arg] == 1:
      print("process wrapping activated")
      ld_preload.append(process_wrapper_path_lib)

  # Create temporary script
  fd, path = tempfile.mkstemp(text=True)
  with os.fdopen(fd, 'w') as fp:
    script = script_template.format(
      ld_preload = " ".join(ld_preload),
      sw_dir     = arguments["--sw_dir"],
      sw_iface   = arguments["--sw_iface"],
      sw_dgb     = arguments["--sw_dgb"],
      command    = " ".join(arguments["<command_line>"])
    )
    fp.write(script)

  # Execute the script with bash
  os.system("bash " + path)
